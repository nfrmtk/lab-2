

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

pub fn t1(poly: & Vec<f64>) -> usize {
    poly.len() - 1
}

pub fn t2(poly: &Vec<f64>) -> bool { // if polynomial has non-complex root
    poly.len() % 2 == 0
}

pub fn t3(poly: &Vec<f64>) -> (f64, f64)  {
    let mut a = poly[poly.len() - 1].abs();
    let mut b = poly[0].abs();
    for i in 0..poly.len() {
        if i != poly.len() - 1 {
            b = b.max(poly[i].abs());
        }
        if i != 0 {
            a = a.max(poly[i].abs());
        }
    }
    return (1.0/(1.0+ b /poly[poly.len() - 1]), 1.0+ a /poly[0]) // 1/(1+2/3) = 3/5, 1 + 3/2 = 5/2
}

pub fn t4(poly: & mut Vec<f64> ) -> f64{
    let mut c = 0.0 as f64;
    let mut i = usize::MAX;

    if poly[0] < 0.0 {
        for j in 0..poly.len() {
            poly[j]*=-1.0;
        }
    }

    for j in 0..poly.len(){
        if poly[j] < 0.0 {
            if i == usize::MAX{
                i = j;
            }
            c = c.min(poly[j]);
        }
    }
    return 1.0 + (c.abs()/poly[0]).powf(1.0/i as f64);
}

pub fn make_polynomial_of_minus_x(poly : & Vec<f64>) -> Vec<f64>{
    let mut answer = poly.clone();
    for i in 0..answer.len(){
        if (answer.len() - i )%2 !=1{
            answer[i]*=-1.0;
        }
    }
    return answer;
}

pub fn t5(poly: & mut Vec<f64> ) -> (f64, f64, f64, f64) {
    let mut poly2 = poly.clone();
    poly2.reverse();
    let mut poly3 = make_polynomial_of_minus_x(poly);
    let mut poly4 = poly3.clone();
    poly4.reverse();

    return (t4(poly), t4(&mut poly2), t4(&mut poly3), t4(&mut poly4))
}



pub fn t6(poly: & Vec<f64>) -> (usize, usize){
    let poly2 = make_polynomial_of_minus_x(poly);
    let (mut s1,mut s2) = (0,0);
    println!("{:?}", poly2);
    for i in 1..poly2.len() {
        if poly[i]*poly[i-1] < 0.0 {
            s1+= 1;
        }
        if poly2[i]*poly2[i-1] < 0.0 {
            s2+= 1;
        }
    }
    return (s1,s2)
}

pub fn t7(poly: &mut Vec<f64>) -> bool{
    for i in 1..poly.len() - 1 {
        if poly[i-1]*poly[i+1] >= poly[i].powi(2){
            return false;
        }
    }
    return true
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t1_test() {
        let result = t1( &vec![1, 2, 3]);
        assert_eq!(result, 2);
    }
    #[test]
    fn t2_test(){
        let result = t2(&vec![1.0, 2.0, 3.0]);
        assert_eq!(result, false)
    }
    #[test]
    fn t3_test(){
        let result = t3(&vec![1.0, 2.0, 3.0]);
        let _ = result.1.round();
        let _ = result.0.round();
        println!("hello!\n my examples for theorem 3 are: {} == 0.6, {} == 4",result.0, result.1 );
        assert!(true);
    }

    #[test]
    fn t4_test(){
        let result = t4(&mut vec![-1.0, 2.0, 3.0]);
        println!("hello!\n my examples for theorem 4 are: {} == 3",result);
        assert!(true);
    }

    #[test]
    fn t5_test(){
        let result = t5(&mut vec![-1.0, 2.0, 3.0]);
        println!("{}, {}, {}, {}", result.0, result.1, result.2, result.3);
    }

    #[test]
    fn t6_test(){
        let result = t6(&vec![-1.0, 2.0, 3.0]);
        assert_eq!(result, (1, 1))
    }

    #[test]
    fn t7_test(){
        assert!(t7(&mut vec![-1.0, -2.0, -3.0]));
        assert!(!t7(&mut vec![-1.0, -2.0, -7.0]));

    }
}
