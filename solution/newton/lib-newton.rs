
use std::vec::Vec;
pub fn at(poly: &Vec<f64>, point: f64) -> f64{
    let mut ans = 0.0;
    let mut current = 1.0;
    for i in 0..poly.len() {
        ans += poly[i]*current;
        current *= point;
        //println!("ans after {} iteration is {}", i, ans);
    }
    return ans;

}

pub fn polynomial_derivative(poly: Vec<f64>) -> Vec<f64>{
    let mut poly = poly;
    for i in 0..poly.len() - 1 {
        poly[i] = (i+1) as f64 *poly[i+1];
    }
    poly.resize(poly.len() - 1, 3.14);
    return poly;
}

pub fn basic_newton(poly: &Vec<f64>, start: f64) -> f64{
    return newton_broiden(poly, start, 1.0);
}

pub fn newton_broiden(poly: &Vec<f64>, start: f64, coefficient: f64) -> f64{
    let derivative = polynomial_derivative(poly.clone());
    let mut zero_point = start;
    while at(&poly, zero_point).abs() > 0.00001 {
        zero_point = zero_point - coefficient * at(&poly, zero_point)/at(&derivative, zero_point);
    }
    return zero_point;
}

pub fn simplified_newton(poly: &Vec<f64>, start: f64) -> f64{
    let derivative = polynomial_derivative(poly.clone());
    let mut zero_point = start;
    while at(&poly, zero_point).abs() > 0.00001 {
        zero_point = zero_point - at(&poly, zero_point)/at(&derivative, start);
    }
    return zero_point;
}

pub fn secants_method(poly: &Vec<f64>, start: f64) -> f64 {
    let mut zero_point = start;
    let mut previous = start - 1.0;
    while at(&poly, zero_point).abs() > 0.00001 {
        let fraction = ( at(poly, zero_point) - at(poly, previous) )/ (zero_point - previous);
        previous = zero_point;
        zero_point = zero_point - at(&poly, zero_point)/fraction;
    }
    return zero_point;
}

pub fn divide_polynomials(poly: &Vec<f64>, divisor: &Vec<f64>) -> Vec<f64> {
    let mut copy = poly.clone();
    let mut ans : Vec<f64> = vec![];
    if divisor.len() > poly.len() {
        return vec![];
    }
    for i in (divisor.len()-1..=copy.len() - 1).rev() {
        let mul = -copy[i] / divisor[divisor.len() - 1];
        ans.push(mul);
        print!("\n");
        for j in 0..divisor.len() {
            copy[i-j] += divisor[divisor.len() - j - 1] * mul;
        }
    }
    ans.reverse();
    return ans;
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn derivative_test(){
        assert_eq!(
            vec![1.0, 2.0, 3.0],
            polynomial_derivative(vec![12123123123.0, 1.0, 1.0, 1.0])
        )
    }
    #[test]
    fn at_test(){
        assert_eq!(
            at(&vec![3.0, 2.0, -1.0], -1.0),
            0.0
        )
    }
    #[test]
    fn basic_newton_test(){
        let poly = vec![3.0, 2.0,-1.0 ];
        println!("this is basic newton method: -1x^2 + 2x + 3 == 0 roots are 3 and -1, lets check, {}, {}",
                 basic_newton(&poly, 2.4823764),
                 basic_newton(&poly, -1.923849)
        );
    }

    #[test]
    fn secants_test(){
        let poly = vec![3.0, 2.0, -1.0];
        println!("this is secants method: -1x^2 + 2x + 3 == 0 roots are 3 and -1, lets check, {}, {}",
                 secants_method(&poly, 2.2123764),
                 secants_method(&poly, -3.773849)
        );
    }
    #[test]
    fn divide_test(){
        let (poly, divisor) = (vec![3.0, 2.0, -1.0], vec![1.0, 1.0]);
        assert_eq!(divide_polynomials(&poly, &divisor), vec![-3.0, 1.0]);
    }

}

